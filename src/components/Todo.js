import React from "react"
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native"

const Todo = ({ item, pressHandler, pressHandlerEdit }) => {

    return (
        <View style={styles.container}>
            {/* <Text>{item.thing}</Text> */}
            <Text style={[item.done ? styles.textDone : styles.textToDo, styles.textCommon]}>{item.thing}</Text>
            <View style={styles.imageIconsContainer}>
                <TouchableOpacity onPress={pressHandler}>
                    {item.done ?
                        <Image style={styles.imageIcon} source={{ uri: require('../../assets/check_circle_icon.png') }} />
                        :
                        <Image style={styles.imageIcon} source={{ uri: require('../../assets/blank_check_circle_icon.png') }} />
                    }
                </TouchableOpacity>
                <TouchableOpacity onPress={pressHandlerEdit}>
                    <Image style={styles.imageIcon} source={{ uri: require('../../assets/create_edit_pencil_write_icon.png') }} />
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flex: 1,
        padding: 5,
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    textCommon: {
        textAlign: 'center'
    },
    textDone: {
        textDecorationLine: "line-through",
        color: '#333'
    },
    textToDo: {
        color: '#000'
    },
    imageIconsContainer: {
        flexDirection: 'row',
        marginLeft: 10
    },
    imageIcon: {
        width: 20,
        height: 20,
        marginLeft: 5,
    }
})

export default Todo