import React, { useEffect, useState } from 'react';
import { View, Text, Button, StyleSheet, TextInput } from 'react-native';

const ItemDetailScreen = ({navigation, route, editHandler, excludeHandler}) => {

  const [todo, setTodo] = useState("");

  return (
    <View style={styles.container}>
      <TextInput
          style={styles.toDoTextInput}
          onChangeText={setTodo}
          value={todo}
          placeholder="Type something to do..."
          // onSubmitEditing={addItem}
        />
        <Button title='Save' />
        <Button title='Delete' />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#aaa',
  },
})

export default ItemDetailScreen;