import React, { useState } from 'react';
import { View, Text, FlatList, Button, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import Todo from '../components/Todo';

const HomeScreen = ({ navigation }) => {

  const [list, setList] = useState([])
  const [id, setId] = useState(0)
  const [todo, setTodo] = useState("")

  const addItem = () => {
    const thing = todo
    setId(prev => prev + 1)
    let newList = list
    const newItem = {
      id: id,
      thing: thing,
      done: false
    }
    newList.push(newItem)
    setList(newList.sort((a, b) => a.done - b.done))
    setTodo("")
  }

  const toggleItem = (id) => {
    let tempList = [...list]
    const itemIndex = tempList.findIndex(item => item.id === id)
    tempList[itemIndex].done = (!tempList[itemIndex].done)
    setList(tempList.sort((a, b) => a.done - b.done))
  }

  const goToDetails = (id) => {
    console.log(id)
    navigation.navigate('Item details')
  }

  // const removeItem = (id) => {
  //   let tempList = list
  // }

  return (
    <View style={styles.container}>
      <Text>This is a list</Text>
      <FlatList
        style={styles.listContainer}
        data={list}
        keyExtractor={item => item.id}
        renderItem={({ item }) => <Todo item={item} pressHandler={() => toggleItem(item.id)} pressHandlerEdit={() => goToDetails(item.id)} />}
      />

      <View style={styles.addThingToDoContainer}>
        <TextInput
          style={styles.toDoTextInput}
          onChangeText={setTodo}
          value={todo}
          placeholder="Type something to do..."
          onSubmitEditing={addItem}
        />
        <Button
          style={styles.toDoAddButton}
          title='Add ToDo'
          onPress={() => addItem()}
        />

      </View>
      {/* <Button
        title='Vai para Details'
        onPress={() => navigation.navigate('ItemDetail')}
      /> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#aaa',
  },
  listContainer: {
    padding: 10,
    // backgroundColor: 'red',
    width: '100%',
  },
  addThingToDoContainer: {
    flexDirection: 'row',
    width: '100%',
  },
  toDoTextInput: {
    flex: 10,
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: '#ddd',
    width: '100%',
  },
  toDoAddButton: {
    flex: 1,
    backgroundColor: '#fff'
  },
})

export default HomeScreen;