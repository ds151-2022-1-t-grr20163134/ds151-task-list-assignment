import React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './src/screens/HomeScreen';
import ItemDetailScreen from './src/screens/ItemDetailScreen';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Things to do" component={HomeScreen} />
        <Stack.Screen name="Item details" component={ItemDetailScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;